from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation


class PresentationListEncoder(ModelEncoder):
    """
    need docstring
    """

    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    """
    need docstring
    """

    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


def api_list_presentations(request, conference_id):
    """
    need to update
    """
    presentations = Presentation.objects.filter(conference=conference_id)
    return JsonResponse(
        {"presentations": presentations},
        encoder=PresentationListEncoder,
        safe=False,
    )


def api_show_presentation(request, id):
    """
    Need a new docstring
    """
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
